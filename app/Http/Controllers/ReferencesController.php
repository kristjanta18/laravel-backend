<?php

namespace App\Http\Controllers;

use App\references;
use App\Group;
use Illuminate\Http\Request;

class ReferencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $references = references::all();
        return view('reference.index',  compact('references'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Group $group)
    {
        $groups = Group::all();
        return view('reference.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Group $groups)
    {
        $reference = new references();
        $reference->group = $request['group'];
        $reference->url = $request['url'];
        $reference->title = $request['title'];
        $reference->save();
        return redirect('../public/references');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(references $reference)
    {
        return view('reference.show',  compact('reference'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(references $reference, Group $groups)
    {
        $groups = Group::all();
        return view('reference.edit',  compact('reference', 'groups'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,references $reference , Group $groups)
    {
        $reference->group = $request['group'];
        $reference->url = $request['url'];
        $reference->title = $request['title'];
        $reference->save();
        return redirect('../public/references');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(references $reference)
    {
        $reference->delete();

        return redirect('../public/references');
    }
}
