@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Service [{{ $service->title }}]</h1>
        <form method="POST" action="{{ route('services.update', $service->id) }}">
            {{ csrf_field() }}
            @method('PUT')
            <div class="form-group">
                <input type="text" name="title" value="{{ $service->title }}">
            </div>
                <div class="form-group">
                <input type="text" name="image" value="{{ $service->image }}">
                </div>
                    <div class="form-group">
                <input type="text" name="content" value="{{ $service->content }}">
            </div>
            <a href="/webpro/public/services" class="btn btn-success">Back</a>
            <input type="submit" value="Submit" class="btn btn-primary">

        </form>
        <form action="{{ route('services.destroy', $service->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger mt-2" type="submit">Delete</button>
        </form>
    </div>


@endsection
