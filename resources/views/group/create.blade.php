@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Add Group</h1>
        <form method="POST" action="{{ route('groups.store') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <h3>Group Title</h3>
                <input type="text" name="group_title" value="" placeholder="title">
            </div>
            <input type="submit" value="Submit" class="btn btn-primary">
        </form>
    </div>
@endsection
