@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit [{{ $group->group_title }}]</h1>
        <form method="POST" action="{{ route('groups.update', $group->id) }}">
            {{ csrf_field() }}
            @method('PUT')
            <div class="form-group">
                <h3>Group_title</h3>
                <input type="text" name="group_title" value="{{ $group->group_title }}">
            <a href="/webpro/public/groups" class="btn btn-success">Back</a>
            <input type="submit" value="Submit" class="btn btn-primary">

        </form>
        <form action="{{ route('groups.destroy', $group->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger mt-2" type="submit">Delete</button>
        </form>
    </div>


@endsection
