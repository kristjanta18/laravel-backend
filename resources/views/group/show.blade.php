@extends('layouts.app')

@section('content')
    <div class="container">
        <p><span>Id: </span>{{ $group->id }}</p>
        <p><span>Group_Title: </span>{{ $group->group_title }}</p>
        <a href="{{ URL('groups/')}}" class="btn btn-danger">Back</a>
    </div>
@endsection
