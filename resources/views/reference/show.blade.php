@extends('layouts.app')

@section('content')
    <div class="container">
        <p><span>Id: </span>{{ $reference->id }}</p>
        <p><span>Group Number: </span>{{ $reference->group }}</p>
        <p><span>Url: </span>{{ $reference->url }}</p>
        <p><span>Title: </span>{{ $reference->title }}</p>
        <a href="{{ URL('references/')}}" class="btn btn-danger">Back</a>
    </div>
@endsection
