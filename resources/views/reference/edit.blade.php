@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit [{{ $reference->title }}]</h1>
        <form method="POST" action="{{ route('references.update', $reference->id) }}">
            {{ csrf_field() }}
            @method('PUT')
            <div class="form-group">
                <h3>Group</h3>
                <select type="text" name="group">
                    @foreach($groups as $group)
                        <option type="text" name="group"  value="{{ $group->id }}">{{ $group->group_title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <h3>Url</h3>
                <input type="text" name="url" value="{{ $reference->url }}">
            </div>
            <div class="form-group">
                <h3>Title</h3>
                <input type="text" name="title" value="{{ $reference->title }}">
            </div>
            <a href="/webpro/public/references" class="btn btn-success">Back</a>
            <input type="submit" value="Submit" class="btn btn-primary">

        </form>
        <form action="{{ route('references.destroy', $reference->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger mt-2" type="submit">Delete</button>
        </form>
    </div>


@endsection
