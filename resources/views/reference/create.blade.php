@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Add References</h1>
        <form method="POST" action="{{ route('references.store') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <h3>Group</h3>
                <select type="text" name="group">
                @foreach($groups as $group)
                    <option type="text" name="group"  value="{{ $group->group_title }}">{{ $group->group_title }}</option>
                @endforeach
                </select>
            </div>
            <div class="form-group">
                <h3>Url</h3>
                <input type="text" name="url" value="" placeholder="url">
            </div>
            <div class="form-group">
                <h3>Title</h3>
                <input type="text" name="title" value="" placeholder="title">
            </div>
            <input type="submit" value="Submit" class="btn btn-primary">
        </form>
    </div>
@endsection

