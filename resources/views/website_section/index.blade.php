@extends('layouts.app')

@section('content')

    <div class="container">
        <table>
            <th>Section Name</th>
            <th><a href="{{ URL('api/website_sections')}}" class="btn btn-danger">Api</a></th>
        </table>
        @foreach($website_sections as $website_section)
            <table>
                <tr>
                    <td>{{  $website_section->title }}</td>
                    <td><a href="{{ URL('website_sections/'.$website_section->id )}}" class="btn btn-success">Info</a></td>
                    <td><a href="{{ URL('website_sections/'.$website_section->id.'/edit/' )}}" class="btn btn-warning">Edit</a></td>
                    <td><form action="{{ route('website_sections.destroy', $website_section->id)}}" method="post">
                            @csrf
                            @method('DELETE')

                            <button class="btn btn-danger" type="submit">Delete</button>

                        </form></td>
                </tr>
            </table>

        @endforeach
    </div>


@endsection
