@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Service [{{ $website_section->title }}]</h1>
        <form method="POST" action="{{ route('services.update', $website_section->id) }}">
            {{ csrf_field() }}
            @method('PUT')
            <div class="form-group">
                <input type="text" name="menu_title" value="{{ $website_section->menu_title }}">
            </div>
            <div class="form-group">
                <input type="text" name="title" value="{{ $website_section->title }}">
            </div>
            <div class="form-group">
                <input type="text" name="content" value="{{ $website_section->content }}">
            </div>
            <a href="/webpro/public/website_sections" class="btn btn-success">Back</a>
            <input type="submit" value="Submit" class="btn btn-primary">

        </form>
        <form action="{{ route('website_sections.destroy', $website_section->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger mt-2" type="submit">Delete</button>
        </form>
    </div>


@endsection
